require 'puppet/util'
require 'fileutils'

Puppet::Type.type(:pyenv).provide(:pyenv) do
  include Puppet::Util::Execution

  def pyenv(c)
    c.unshift(sprintf("%s/bin/pyenv", @resource[:install_dir]))

    output = nil

    output = Puppet::Util::Execution.execute(
      c,
      {
        :uid => @resource[:runas],
        :custom_environment => { 'PYENV_ROOT' => @resource[:install_dir] },
        :failonfail => true
      }
    )
  end

  ## Puppet required methods
  def exists?
    if (File.exists?("#{@resource[:install_dir]}/versions/#{@resource[:name]}"))
      return true
    else
      return false
    end
  end

  def create
    command = ['install']
    command.push('-s')

    if resource[:keep] == :true
      command.push('--keep')
    end

    if resource[:name].split('-').last == 'debug'
      command.push('--debug')
      command.push(resource[:name].split('-')[0..-1])
    else
      command.push(resource[:name])
    end

    notice("Attempting to install #{@resource[:name]} from user #{@resource[:runas]}. This might take a while...")
    pyenv(command)
  end

  def destroy
    command = ['uninstall']
    command.push('-f')
    command.push(@resource[:name])
    pyenv(command)
  end
end
