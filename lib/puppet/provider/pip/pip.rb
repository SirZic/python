require 'puppet/util'
require 'fileutils'
require 'json'

Puppet::Type.type(:pip).provide(:pip) do
  include Puppet::Util::Execution

  def getRoot
    if @resource[:virtualenv]
      return @resource[:virtualenv]
    else
      return sprintf("%s/versions/%s", @resource[:pyenv_root], @resource[:python_version])
    end
  end

  def getPip
    return sprintf("%s/bin/pip", getRoot)
  end

  def pip(c)
    c.unshift(getPip)

    output = nil

    output = Puppet::Util::Execution.execute(
      c,
      {
        :uid => @resource[:runas],
        :custom_environment => { 'PYENV_ROOT' => @resource[:pyenv_root] },
        :failonfail => true
      }
    )
  end

  def getList
    list = Hash.new()
    command = ['list']
    command.push('--format')
    command.push('json')
    json = JSON.parse(pip(command))
    json.each { |s|
      list[s['name']] = s['version']
    }

    return list
  end

  def pkgExists?
    list = getList
    list.has_key? @resource[:pkgname]
  end

  ## Puppet required methods
  def exists?
    pkgExists?
  end

  def create
    command = ['install']
    command.push(@resource[:pkgname])

    notice("Installing pip package: #{@resource[:pkgname]} from user #{@resource[:runas]} is. This might take a while...")
    pip(command)
  end

  def destroy
    command = ['uninstall']
    command.push(@resource[:pkgname])
    pip(command)
  end
end
