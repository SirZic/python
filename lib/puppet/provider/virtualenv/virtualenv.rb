require 'puppet/util'
require 'fileutils'

Puppet::Type.type(:virtualenv).provide(:virtualenv) do
  include Puppet::Util::Execution

  def virtualenv(c)
    c.unshift(sprintf("%s/versions/%s/bin/virtualenv", @resource[:pyenv_root], @resource[:python_version]))

    output = nil

    output = Puppet::Util::Execution.execute(
      c,
      {
        :uid => @resource[:runas],
        :custom_environment => { 'PYENV_ROOT' => @resource[:pyenv_root] },
        :failonfail => true
      }
    )
  end

  ## Puppet required methods
  def exists?
    File.exists?(@resource[:name]) && File.exists?(sprintf("%s/%s", @resource[:name], '.python-version')) && File.exists?(sprintf("%s/%s", @resource[:name], '/bin/activate'))
  end

  def create
    FileUtils::mkdir_p @resource[:name]
    f = File.new(sprintf("%s/%s", @resource[:name], '.python-version'), 'w')
    f.write(@resource[:python_version] + "\n")
    f.close
    FileUtils.chown_R @resource[:user], @resource[:group], @resource[:name], :verbose => true

    command = [@resource[:name]]

    notice("Creating vritualenv: #{@resource[:name]} from user #{@resource[:runas]}")
    virtualenv(command)
  end

  def destroy
    FileUtils.rm_rf(@resource[:name])
  end
end
