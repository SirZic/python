# == Class: python::config
# Author: Siebren Zwerver
#

class python::config (
  $ensure      = present,
  $bashrc      = $python::bashrc,
  $user        = $python::user,
  $install_dir = $python::pyenv_install_dir,
) {

  file { $bashrc:
    ensure => $ensure,
  }
  -> file_line { 'pyenv export root':
    ensure => $ensure,
    path   => $bashrc,
    line   => "export PYENV_ROOT=\"${install_dir}\""
  }
  -> file_line { 'pyenv export path':
    ensure => $ensure,
    path   => $bashrc,
    line   => "export PATH=${install_dir}/bin:\${PATH}",
    after  => '^export PYENV_ROOT'
  }
  -> file_line { 'pyenv init':
    ensure => $ensure,
    path   => $bashrc,
    line   => 'eval "$(pyenv init -)"',
    after  => "^export PATH=${install_dir}"
  }
}
